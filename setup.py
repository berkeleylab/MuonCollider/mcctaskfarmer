#!/usr/bin/env python

import pathlib
import setuptools

# The directory containing this file
HERE = pathlib.Path(__file__).parent

# The text of the README file
README = (HERE / "README.md").read_text()

setuptools.setup(
    name='mcctaskfarmer',
    version='0.0.1',
    description='Extensions to PyTaskFarmer to use with the MCC framework.',
    long_description=README,
    long_description_content_type='text/markdown',
    url='https://gitlab.cern.ch/berkeleylab/MuonCollider/mcctaskfarmer',
    packages=['mcctaskfarmer'],
    install_requires=['PySIO']
    )
